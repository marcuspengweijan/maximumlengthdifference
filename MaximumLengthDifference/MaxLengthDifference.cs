﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumLengthDifference
{
    public class MaxLengthDifference : MaxLengthDifferenceBase
    {
        public MaxLengthDifference(string[] a1, string[] a2)
        {
            A1 = a1;
            A2 = a2;
        }
    }
}
