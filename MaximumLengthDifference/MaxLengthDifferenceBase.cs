﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumLengthDifference
{
    public abstract class MaxLengthDifferenceBase
    {
        public string[] A1 { get; protected set; }
        public string[] A2 { get; protected set; }

        public int Mxdiflg => (A1 == null || A2 == null || A1.Length.Equals(0) || A2.Length.Equals(0))
            ? -1
            : Math.Max(A1.Max(x => x.Length) - A2.Min(x => x.Length),
                A2.Max(x => x.Length) - A1.Min(x => x.Length));
    }
}
