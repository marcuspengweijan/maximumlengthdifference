﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumLengthDifference
{
    public class Kata
    {
        public static int Mxdiflg(string[] a1, string[] a2)
        {
            if (a1 == null || a2 == null || a1.Length.Equals(0) || a2.Length.Equals(0)) return -1;

            /* Method 1 */

            //var a1Tuple = (max: a1.Max(x => x.Length), min: a1.Min(x => x.Length));
            //var a2Tuple = (max: a2.Max(x => x.Length), min: a2.Min(x => x.Length));

            //return Math.Max(a1Tuple.max - a2Tuple.min, a2Tuple.max - a1Tuple.min);


            /* Method 2 */

            //Tuple<int, int> a1Tuple = new Tuple<int, int>(a1.Max(x => x.Length), a1.Min(x => x.Length));
            //Tuple<int, int> a2Tuple = new Tuple<int, int>(a2.Max(x => x.Length), a2.Min(x => x.Length));

            //return Math.Max(a1Tuple.Item1 - a2Tuple.Item2, a2Tuple.Item1 - a1Tuple.Item2);


            /* Method 3 */

            return Math.Max(a1.Max(x => x.Length) - a2.Min(x => x.Length),
                a2.Max(x => x.Length) - a1.Min(x => x.Length));
        }
    }
}
