﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaximumLengthDifference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumLengthDifference.Tests
{
    [TestClass]
    public class MaxLengthDifferenceTests
    {
        [TestMethod]
        public void MxdiflgTest()
        {
            string[] s1 = new string[] { "hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz" };
            string[] s2 = new string[] { "cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww" };
            Assert.AreEqual(13, new MaxLengthDifference(s1, s2).Mxdiflg);
        }

        [TestMethod]
        public void MxdiflgTest_Null()
        {
            string[] s2 = new string[] { "cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww" };
            Assert.AreEqual(-1, new MaxLengthDifference(null, s2).Mxdiflg);
        }

        [TestMethod]
        public void MxdiflgTest_Empty()
        {
            string[] s1 = new string[] { };
            string[] s2 = new string[] { "cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww" };
            Assert.AreEqual(-1, new MaxLengthDifference(s1, s2).Mxdiflg);
        }

        [TestMethod]
        public void MxdiflgTest_Same()
        {
            string[] s1 = new string[] { "cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww" };
            string[] s2 = new string[] { "cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww" };
            Assert.AreEqual(6, new MaxLengthDifference(s1, s2).Mxdiflg);
        }

        [TestMethod]
        public void MxdiflgTest_SameLength()
        {
            string[] s1 = new string[] { "aaaaa", "aaaaa", "aaaaa" };
            string[] s2 = new string[] { "aaaaa", "aaaaa", "aaaaa" };
            Assert.AreEqual(0, new MaxLengthDifference(s1, s2).Mxdiflg);
        }

        [TestMethod]
        public void MxdiflgTest_MaxMinSameSide()
        {
            string[] s1 = new string[] { "a", "aaaaaaaaaaaaaaaaaaaa", "aaaaa" };
            string[] s2 = new string[] { "aaaaa", "aaaaa", "aaaaa" };
            Assert.AreEqual(15, new MaxLengthDifference(s1, s2).Mxdiflg);
        }
    }
}
